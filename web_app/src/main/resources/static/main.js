(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/Utils/cuerpo/cuerpo.component.css":
/*!***************************************************!*\
  !*** ./src/app/Utils/cuerpo/cuerpo.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Utils/cuerpo/cuerpo.component.html":
/*!****************************************************!*\
  !*** ./src/app/Utils/cuerpo/cuerpo.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class='divTitulo'>\r\n  <h5>\r\n    <span class='align-middle'>\r\n      <span>Inicio</span>\r\n    </span>\r\n  </h5>\r\n</div>\r\n<div class='divCuerpo'>\r\n    <img src=\"assets/img/Dentegrin.png\" style=\"width: 500px;height: 500px;\">\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/Utils/cuerpo/cuerpo.component.ts":
/*!**************************************************!*\
  !*** ./src/app/Utils/cuerpo/cuerpo.component.ts ***!
  \**************************************************/
/*! exports provided: CuerpoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CuerpoComponent", function() { return CuerpoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _datos_model_ts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./datos.model.ts */ "./src/app/Utils/cuerpo/datos.model.ts.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CuerpoComponent = /** @class */ (function () {
    function CuerpoComponent(modalService) {
        this.modalService = modalService;
        this.objValidador = new Validadores(false);
        this.strMensaje = "Mensaje informativo.";
        this.strMensajeError = "Mensaje de error.";
        this.strMensajeExito = "Mensaje de &eacute;xito.";
    }
    CuerpoComponent.prototype.ngOnInit = function () {
        this.strResultado = new Array();
        this.strResultado.push(this.agregar("MX00001", "Prueba 1", "ACTIVO"));
        this.strResultado.push(this.agregar("MX00002", "Prueba 2", "ACTIVO"));
        this.strResultado.push(this.agregar("MX00003", "Prueba 3", "ACTIVO"));
    };
    CuerpoComponent.prototype.cambiarObligatorio = function () {
        this.objValidador.bolDato = !(this.objValidador.bolDato);
    };
    CuerpoComponent.prototype.abrirModal = function (modal) {
        var closeResult;
        var options = { size: "dialog-centered" };
        this.modalService.open(modal, options).result.then(function (result) {
            closeResult = "" + result;
            console.log(closeResult);
        }, function (reason) {
        });
    };
    CuerpoComponent.prototype.agregar = function (a, b, c) {
        var nuevoDato = new _datos_model_ts__WEBPACK_IMPORTED_MODULE_2__["Datos"];
        nuevoDato.USUARIO = a;
        nuevoDato.NOMBRE = b;
        nuevoDato.IDSITUACION = c;
        return nuevoDato;
    };
    CuerpoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cuerpo',
            template: __webpack_require__(/*! ./cuerpo.component.html */ "./src/app/Utils/cuerpo/cuerpo.component.html"),
            styles: [__webpack_require__(/*! ./cuerpo.component.css */ "./src/app/Utils/cuerpo/cuerpo.component.css")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbModal"]])
    ], CuerpoComponent);
    return CuerpoComponent;
}());

var Validadores = /** @class */ (function () {
    function Validadores(bolDato) {
        this.bolDato = bolDato;
    }
    return Validadores;
}());


/***/ }),

/***/ "./src/app/Utils/cuerpo/datos.model.ts.ts":
/*!************************************************!*\
  !*** ./src/app/Utils/cuerpo/datos.model.ts.ts ***!
  \************************************************/
/*! exports provided: Datos */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Datos", function() { return Datos; });
var Datos = /** @class */ (function () {
    function Datos() {
    }
    return Datos;
}());



/***/ }),

/***/ "./src/app/Utils/encabezado/encabezado.component.css":
/*!***********************************************************!*\
  !*** ./src/app/Utils/encabezado/encabezado.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".EncabezadoInfo{\r\n\tmax-width: 1000px;\r\n\tmin-width: 700px;\r\n\tfloat: none;\r\n    margin: 0 auto;\r\n}\r\n.imgEncabezado{\r\n\tmargin-left: 0.5rem;\r\n\tmargin-top: 0.1rem;\r\n\theight: auto; \r\n\twidth: auto; \r\n\t/*max-width: 178px; */\r\n\tmax-height: 3.5rem;\r\n}\r\n.iconoEncabezado{\r\n    width: 2.5rem !important;\r\n    height: 2.5rem !important;\r\n}\r\n.datosUsuario{\r\n\tcolor:#FEFEFE;\r\n\tmin-width: 14rem;\r\n\tmargin-top: -0.3rem;\r\n}\r\n.maxHeight{\r\n\theight: 100%;\r\n}\r\n.ControlEnc{\r\n\tvertical-align: middle;\r\n\tmargin-top: 0.8rem;\r\n\tmargin-right: 0.9rem;\r\n}\r\n"

/***/ }),

/***/ "./src/app/Utils/encabezado/encabezado.component.html":
/*!************************************************************!*\
  !*** ./src/app/Utils/encabezado/encabezado.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"align-middle maxHeight EncabezadoInfo\">\n  <img class=\"imgEncabezado\" src=\"assets/img/logoA.png\" />\n  <div class=\"float-right maxHeight\">\n    <span class=\"float-left ControlEnc\"><a href=\"{{strUrlInicio}}\"><img class=\"iconoEncabezado\" src=\"assets/img/Inicio.png\" placement=\"left\" ngbTooltip=\"Ir a inicio\"/></a></span>\n    <span class=\"float-left ControlEnc\">\n      <p class=\"datosUsuario\">\n        <span [innerHTML]=\"strUsuario\"></span><br>\n        <span [innerHTML]=\"strPuesto\"></span>\n      </p>\n    </span>\n    <span class=\"float-left ControlEnc\"><img class=\"iconoEncabezado\" src=\"assets/img/Usuario.png\" /></span>\n    <span class=\"float-left ControlEnc\"><img class=\"iconoEncabezado\" src=\"assets/img/Logout.png\" placement=\"left\" ngbTooltip=\"Cerrar Sesión\"/></span>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Utils/encabezado/encabezado.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/Utils/encabezado/encabezado.component.ts ***!
  \**********************************************************/
/*! exports provided: EncabezadoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EncabezadoComponent", function() { return EncabezadoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _constantes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../constantes */ "./src/app/constantes.ts");
/* harmony import */ var _errores_errores_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../errores/errores.component */ "./src/app/Utils/errores/errores.component.ts");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../app.service */ "./src/app/app.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EncabezadoComponent = /** @class */ (function () {
    function EncabezadoComponent(http, SesionService) {
        var _this = this;
        this.http = http;
        this.SesionService = SesionService;
        this.strUrlInicio = _constantes__WEBPACK_IMPORTED_MODULE_2__["Constantes"].Inicio;
        this.cmpError = new _errores_errores_component__WEBPACK_IMPORTED_MODULE_3__["ErroresComponent"]();
        this.subscriptionIdUsrLogin = SesionService.idUsuarioLogin$.subscribe(function (idUsuarioLogin) {
            _this.strIdUsuarioLogin = idUsuarioLogin;
        });
        this.subscriptionIdUsrLogin = SesionService.objMenu$.subscribe(function (objMenu) {
            _this.objMenu = objMenu;
        });
    }
    EncabezadoComponent.prototype.ngOnInit = function () {
        this.obtenerDatos();
    };
    EncabezadoComponent.prototype.obtenerDatos = function () {
        var _this = this;
        var jsonRespuesta;
        this.strMensaje = "";
        if (true) {
            this.http.get(_constantes__WEBPACK_IMPORTED_MODULE_2__["Constantes"].SRV_ObtenerDatosEncabezado).subscribe(function (data) {
                jsonRespuesta = data;
                if (jsonRespuesta.length != 0) {
                    _this.strUsuario = JSON.parse(jsonRespuesta.DATOS[0].USUARIO).nombre;
                    _this.strPuesto = JSON.parse(jsonRespuesta.DATOS[0].USUARIO).puesto;
                    _this.strIdUsuarioLogin = JSON.parse(jsonRespuesta.DATOS[0].USUARIO).idUsuario;
                    _this.objMenu = jsonRespuesta.DATOS[0].MENU;
                    _this.SesionService.asignarUsuarioLogeado(_this.strIdUsuarioLogin);
                    _this.SesionService.establecerMenu(_this.objMenu);
                }
            }, function (err) {
                _this.cmpError.manejoErroresWS(err);
            });
        }
        this.strUsuario = this.strUsuario != null ? this.strUsuario : "Nombre del Usuario";
        this.strPuesto = this.strPuesto != null ? this.strPuesto : "Puesto del Usuario";
    };
    EncabezadoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-encabezado',
            template: __webpack_require__(/*! ./encabezado.component.html */ "./src/app/Utils/encabezado/encabezado.component.html"),
            styles: [__webpack_require__(/*! ./encabezado.component.css */ "./src/app/Utils/encabezado/encabezado.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _app_service__WEBPACK_IMPORTED_MODULE_4__["DatosSesionService"]])
    ], EncabezadoComponent);
    return EncabezadoComponent;
}());



/***/ }),

/***/ "./src/app/Utils/errores/errores.component.css":
/*!*****************************************************!*\
  !*** ./src/app/Utils/errores/errores.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Utils/errores/errores.component.html":
/*!******************************************************!*\
  !*** ./src/app/Utils/errores/errores.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Utils/errores/errores.component.ts":
/*!****************************************************!*\
  !*** ./src/app/Utils/errores/errores.component.ts ***!
  \****************************************************/
/*! exports provided: ErroresComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErroresComponent", function() { return ErroresComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constantes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../constantes */ "./src/app/constantes.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ErroresComponent = /** @class */ (function () {
    function ErroresComponent() {
    }
    ErroresComponent.prototype.ngOnInit = function () {
    };
    ErroresComponent.prototype.manejoErroresWS = function (err) {
        var strError = '';
        if (err.error instanceof Error) {
            console.log('Problema en el cliente.' + err.status);
            strError = 'Problema en el cliente.' + err.status;
        }
        else {
            if (err.status == 404) {
                console.log('Error 404, recurso no encontrado');
                strError = 'Error 404, recurso no encontrado';
            }
            else if (err.status == 401 || err.status == 403) {
                window.alert("Ha terminado su sesión, favor de ingresar nuevamente.");
                strError = "Ha terminado su sesión, favor de ingresar nuevamente.";
                window.location.href = _constantes__WEBPACK_IMPORTED_MODULE_1__["Constantes"].InicioLogout;
            }
            else {
                console.log('Problema en el servidor. N.(' + err.status + ');Mensaje.(' + err.message + ')');
                strError = 'Problema en el servidor. N.(' + err.status + ');Mensaje.(' + err.message + ')';
            }
        }
        return strError;
    };
    ErroresComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-errores',
            template: __webpack_require__(/*! ./errores.component.html */ "./src/app/Utils/errores/errores.component.html"),
            styles: [__webpack_require__(/*! ./errores.component.css */ "./src/app/Utils/errores/errores.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ErroresComponent);
    return ErroresComponent;
}());



/***/ }),

/***/ "./src/app/Utils/inicio/inicio.component.css":
/*!***************************************************!*\
  !*** ./src/app/Utils/inicio/inicio.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*azul dentegra:; #0075c9*/\r\n/*color gris: cfd8dc*/\r\n.Encabezado{\r\n\theight: 4rem;\r\n\twidth: 100%;\r\n\t/*background: #cfd8dc;*/\r\n\tbackground: #0075c9;\r\n\tposition: relative;\r\n\tz-index: 2;\r\n\tfloat: left;\r\n\tbox-shadow: 1px 9px 28px -12px rgba(0,0,0,0.75);\r\n\r\n\r\n}\r\n.Contenido{\r\n\twidth: 100%;\r\n\theight: calc(100% - 6rem);\r\n\theight: -o-calc(100% - 6rem);\r\n\tposition: relative;\r\n\tdisplay: inline-flex;\r\n\tfloat: left;\r\n}\r\n.Cuerpo{\r\n\tflex-grow: 1;\r\n\torder: 2;\r\n\toverflow: auto;\r\n\tpadding: 0.6rem;\r\n\theight: 100%;\r\n}\r\n.Menu{\r\n\torder: 1;\r\n\t/*12 ant*/\r\n\tmin-width: 12rem;\r\n\tmax-width: 12rem;\r\n\tbackground: #FFF;\r\n\toverflow: auto;\r\n\tborder-color: #D4D4D4;\r\n\t/*\r\n\t-webkit-box-shadow: 2px 9px 34px -6px rgba(0,0,0,0.75);\r\n\t-moz-box-shadow: 2px 9px 34px -6px rgba(0,0,0,0.75);\r\n\tbox-shadow: 2px 9px 28px -6px rgba(0,0,0,0.75);\r\n\t*/\r\n\tz-index: 0;\r\n}\r\n/**/\r\n.Pie {\r\n\tcolor: #FFF;\r\n\tbackground:  #0075c9;\r\n\tposition:absolute;\r\n\tvertical-align: center;\r\n\tleft:0rem;\r\n\tright:0rem;\r\n\tbottom:0rem;\r\n\theight:2rem;\r\n\tz-index:11;\r\n\ttext-align: center;\r\n}\r\n.Navegador{\r\n\twidth: 100%;\r\n\theight: 100%;\r\n}\r\n.iconoMenu{\r\n    width: 1.5rem !important;\r\n    height: 1.8rem !important;\r\n    margin-top: 0.15rem !important;\r\n    position: fixed;\r\n}\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/Utils/inicio/inicio.component.html":
/*!****************************************************!*\
  !*** ./src/app/Utils/inicio/inicio.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class='Encabezado'><app-encabezado></app-encabezado></div>\n<div class='Contenido colDivmenu' >\n  <mat-drawer-container class=\"Navegador\" hasBackdrop=\"false\">\n    <mat-drawer #MenuLateral mode=\"side\">\n      <div class='docs-component-viewer-nav-content Menu'><app-menu></app-menu></div>\n    </mat-drawer>\n    <mat-drawer-content>\n      <div class='Cuerpo'>\n        <input class=\"iconoMenu\" type=\"image\" src=\"assets/img/Menu.png\" (click)=\"MenuLateral.toggle()\" />\n        <app-cuerpo></app-cuerpo>\n      </div>\n    </mat-drawer-content>\n  </mat-drawer-container>\n</div>\n<div class='Pie'><strong>Dentegra Seguros Dentales M&eacute;xico</strong> &copy; Todos los derechos reservados.</div>"

/***/ }),

/***/ "./src/app/Utils/inicio/inicio.component.ts":
/*!**************************************************!*\
  !*** ./src/app/Utils/inicio/inicio.component.ts ***!
  \**************************************************/
/*! exports provided: InicioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InicioComponent", function() { return InicioComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InicioComponent = /** @class */ (function () {
    function InicioComponent() {
    }
    InicioComponent.prototype.ngOnInit = function () {
    };
    InicioComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-inicio',
            template: __webpack_require__(/*! ./inicio.component.html */ "./src/app/Utils/inicio/inicio.component.html"),
            styles: [__webpack_require__(/*! ./inicio.component.css */ "./src/app/Utils/inicio/inicio.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], InicioComponent);
    return InicioComponent;
}());



/***/ }),

/***/ "./src/app/Utils/menu/menu.component.css":
/*!***********************************************!*\
  !*** ./src/app/Utils/menu/menu.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n"

/***/ }),

/***/ "./src/app/Utils/menu/menu.component.html":
/*!************************************************!*\
  !*** ./src/app/Utils/menu/menu.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-accordion>\n  <ng-container *ngFor=\"let MenuP of strResultado\">\n    <mat-expansion-panel class=\"Panel1\">\n      <mat-expansion-panel-header>\n        <mat-panel-title>\n          <span [innerHTML]=\"MenuP.V | titlecase\"></span>\n        </mat-panel-title>\n      </mat-expansion-panel-header>\n      <ng-container *ngFor=\"let MenuH of MenuP.H\">\n        <span *ngIf=\"!MenuH.N\">\n          <a href=\"{{strUrlActual}}{{MenuH.P}}\"><p class = \"PMenu\" [innerHTML]=\"MenuH.V | titlecase\"></p></a>\n        </span>\n        <span *ngIf=\"MenuH.N\">\n          <mat-expansion-panel class=\"Panel2\">\n            <mat-expansion-panel-header>\n              <mat-panel-title>\n                <span [innerHTML]=\"MenuH.V | titlecase\"></span>\n              </mat-panel-title>\n            </mat-expansion-panel-header>\n            <ng-container *ngFor=\"let MenuN of MenuH.N\">\n              <a href=\"{{strUrlActual}}{{MenuN.P}}\"><p class = \"PMenu\" [innerHTML]=\"MenuN.V | titlecase\"></p></a>\n            </ng-container>\n          </mat-expansion-panel>\n        </span>\n      </ng-container>\n    </mat-expansion-panel>\n  </ng-container>\n</mat-accordion>\n\n<!--\n<mat-accordion>\n  <mat-expansion-panel class=\"Panel1\">\n    <mat-expansion-panel-header>\n      <mat-panel-title>\n        Emisor\n      </mat-panel-title>\n    </mat-expansion-panel-header>\n    <mat-expansion-panel class=\"Panel2\">\n      <mat-expansion-panel-header>\n        <mat-panel-title>\n          Contratantes\n        </mat-panel-title>\n      </mat-expansion-panel-header>\n        <a href=\"EmiCont/\"><p class = \"PMenu\">Contratante</p></a>\n        <a href=\"EmiCP/\"><p class = \"PMenu\">Código Postal</p></a>\n        <p class = \"PMenu\">Personas</p>\n        <a href=\"AngularApp/\"><p class = \"PMenu\">Agrupador</p></a>\n    </mat-expansion-panel>\n  </mat-expansion-panel>\n\n  <mat-expansion-panel class=\"Panel1\">\n    <mat-expansion-panel-header>\n      <mat-panel-title>\n        Seguridad\n      </mat-panel-title>\n    </mat-expansion-panel-header>\n      <a href=\"SegUsr/\"><p class = \"PMenu\">Seguridad</p></a>\n  </mat-expansion-panel>\n</mat-accordion>\n-->"

/***/ }),

/***/ "./src/app/Utils/menu/menu.component.ts":
/*!**********************************************!*\
  !*** ./src/app/Utils/menu/menu.component.ts ***!
  \**********************************************/
/*! exports provided: MenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuComponent", function() { return MenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../app.service */ "./src/app/app.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MenuComponent = /** @class */ (function () {
    function MenuComponent(http, SesionService) {
        var _this = this;
        this.http = http;
        this.SesionService = SesionService;
        this.strError = '';
        /*Menu de emergencia*/
        this.strResultado = JSON.parse('[{"V":"EMISOR","P":"","H":[{"V":"SEGURIDAD","P":"","N":null},{"V":"CONTRATANTES","P":"","N":[{"V":"CONTRATANTE","P":null},{"V":"CÓDIGO POSTAL","P":"/hola/"},{"V":"GIRO","P":null},{"V":"AGRUPADOR","P":null}]},{"V":"PRODUCTOS","P":"","N":[{"V":"CATÁLOGOS","P":null},{"V":"PLANES","P":null},{"V":"PROCEDIMIENTOS","P":null}]}]}]');
        this.strUrlActual = window.location.origin;
        this.subscriptionObjMenu = SesionService.objMenu$.subscribe(function (objMenu) {
            if (objMenu != '')
                _this.strResultado = JSON.parse(objMenu);
        });
    }
    MenuComponent.prototype.ngOnInit = function () {
    };
    MenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-menu',
            template: __webpack_require__(/*! ./menu.component.html */ "./src/app/Utils/menu/menu.component.html"),
            styles: [__webpack_require__(/*! ./menu.component.css */ "./src/app/Utils/menu/menu.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _app_service__WEBPACK_IMPORTED_MODULE_2__["DatosSesionService"]])
    ], MenuComponent);
    return MenuComponent;
}());



/***/ }),

/***/ "./src/app/app-routes.module.ts":
/*!**************************************!*\
  !*** ./src/app/app-routes.module.ts ***!
  \**************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: '', redirectTo: '', pathMatch: 'full' },
    { path: 'appComponent', component: _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]],
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-inicio></app-inicio>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _Utils_menu_menu_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Utils/menu/menu.component */ "./src/app/Utils/menu/menu.component.ts");
/* harmony import */ var _Utils_encabezado_encabezado_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Utils/encabezado/encabezado.component */ "./src/app/Utils/encabezado/encabezado.component.ts");
/* harmony import */ var _Utils_inicio_inicio_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./Utils/inicio/inicio.component */ "./src/app/Utils/inicio/inicio.component.ts");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.service */ "./src/app/app.service.ts");
/* harmony import */ var _app_routes_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app-routes.module */ "./src/app/app-routes.module.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _Utils_cuerpo_cuerpo_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./Utils/cuerpo/cuerpo.component */ "./src/app/Utils/cuerpo/cuerpo.component.ts");
/* harmony import */ var _Utils_errores_errores_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./Utils/errores/errores.component */ "./src/app/Utils/errores/errores.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _Utils_inicio_inicio_component__WEBPACK_IMPORTED_MODULE_7__["InicioComponent"],
                _Utils_menu_menu_component__WEBPACK_IMPORTED_MODULE_5__["MenuComponent"],
                _Utils_encabezado_encabezado_component__WEBPACK_IMPORTED_MODULE_6__["EncabezadoComponent"],
                _Utils_cuerpo_cuerpo_component__WEBPACK_IMPORTED_MODULE_12__["CuerpoComponent"],
                _Utils_errores_errores_component__WEBPACK_IMPORTED_MODULE_13__["ErroresComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _app_routes_module__WEBPACK_IMPORTED_MODULE_9__["AppRoutingModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_10__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatSidenavModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"].forRoot()
            ],
            providers: [_app_service__WEBPACK_IMPORTED_MODULE_8__["DatosSesionService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.service.ts":
/*!********************************!*\
  !*** ./src/app/app.service.ts ***!
  \********************************/
/*! exports provided: DatosSesionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatosSesionService", function() { return DatosSesionService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var DatosSesionService = /** @class */ (function () {
    function DatosSesionService() {
        // Observable string sources
        this.idUsuarioLogin = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]('0');
        this.objMenu = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]('');
        this.idUsuario = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]('0');
        this.IdOrigenSource = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]('');
        // Observable string streams
        this.idUsuarioLogin$ = this.idUsuarioLogin.asObservable();
        this.objMenu$ = this.objMenu.asObservable();
        this.idUsuario$ = this.idUsuario.asObservable();
        this.IdOrigenSource$ = this.IdOrigenSource.asObservable();
    }
    // Service message commands
    DatosSesionService.prototype.asignarUsuarioLogeado = function (idUsuarioLogin) {
        this.idUsuarioLogin.next(idUsuarioLogin);
    };
    DatosSesionService.prototype.establecerMenu = function (objMenu) {
        this.objMenu.next(objMenu);
    };
    DatosSesionService.prototype.establecerUsr = function (IdUsuario) {
        this.idUsuario.next(IdUsuario);
    };
    DatosSesionService.prototype.enviarId = function (IdEnviado) {
        this.IdOrigenSource.next(IdEnviado);
    };
    DatosSesionService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
        /*Clase con el servicio usado para comunicar las paginas por medio de variables.*/
    ], DatosSesionService);
    return DatosSesionService;
}());



/***/ }),

/***/ "./src/app/constantes.ts":
/*!*******************************!*\
  !*** ./src/app/constantes.ts ***!
  \*******************************/
/*! exports provided: Constantes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Constantes", function() { return Constantes; });
/*Constantes con la ruta de los servicios web a consumir.*/
var Constantes = /** @class */ (function () {
    function Constantes() {
    }
    Constantes.IPServidor = function () { return window.location.origin; };
    ;
    Object.defineProperty(Constantes, "Inicio", {
        get: function () { return window.location.origin; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(Constantes, "InicioLogout", {
        get: function () { return window.location.origin + '/login'; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(Constantes, "SRV_ObtenerDatosEncabezado", {
        get: function () { return this.IPServidor() + "/api/usr/"; },
        enumerable: true,
        configurable: true
    });
    ;
    return Constantes;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Antonio\ws\Proyecto\Angular\Inicio\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map