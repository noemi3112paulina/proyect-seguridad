package com.dentegra.web_app.configuration;

import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

@Configuration
@EnableOAuth2Sso
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Override
	public void configure(WebSecurity web) throws Exception {

		web.ignoring().antMatchers("/**/css/**", "/**/fonts/**", "/**/img/**", "/**/js/**");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		/*http.antMatcher("/**").authorizeRequests()
		.antMatchers("/login", "/uaa/**", "/fonts/**", "/js/**", "/uaa/css/**", "/uaa/fonts/**", "/uaa/js/**","/uaa/css/**")
		.permitAll().antMatchers("/home/**").hasAuthority("SUBDIR_OPER").anyRequest().authenticated().and()
		.formLogin().loginPage("/login")
		.permitAll().and().logout().permitAll().deleteCookies("JSESSIONID").invalidateHttpSession(true)
		.and().csrf().disable();*/
		
		http.logout().permitAll().deleteCookies("JSESSIONID").invalidateHttpSession(true)
		.and().antMatcher("/**").authorizeRequests()
        .antMatchers("/login","/auth/**","/uaa/**", "/fonts/**", "/js/**", "/uaa/css/**", "/uaa/fonts/**", "/uaa/js/**","/uaa/css/**","/forbidden").permitAll()
        .anyRequest().authenticated().and().csrf().disable();
        
		
		http.exceptionHandling().accessDeniedPage("/forbidden");
		
	}
	
}
