package com.dentegra.web_app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ZuulController {
	
	@RequestMapping(value="/Error403")
    public String Error403(){
        return "403";
    }
	
	@RequestMapping(value="/home")
    public String home(){
        return "index";
    }

}
