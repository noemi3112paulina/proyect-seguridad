package com.dentegra.microservicio.bean;

import java.util.Map;

public interface UserInfService {
	
	public UserInf getUserInf(String userName); 
	public Map<String, Object> obtenerInformacionUsuario(String userName);
	
}
