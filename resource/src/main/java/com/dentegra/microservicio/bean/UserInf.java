package com.dentegra.microservicio.bean;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInf {
	
	private String idUsuario;
	private	String userName;
	private String nombre;
	private String puesto;
	private String perfil;
	
	
	@JsonCreator
	 public UserInf(@JsonProperty("idUsuario") String idUsuario, @JsonProperty("userName") String userName, @JsonProperty("nombre") String nombre, @JsonProperty("puesto") String puesto,
	   @JsonProperty("perfil") String perfil) {
	  this.idUsuario = idUsuario;
      this.userName = userName;
	  this.nombre = nombre;
	  this.puesto = puesto;
	  this.perfil= perfil;
	 }
	
	public UserInf(){
		
	}
	
	public String getidUsuario() {
		return idUsuario;
	}

	public void setidUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getuserName() {
		return userName;
	}

	public void setuserName(String userName) {
		this.userName = userName;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getPuesto() {
		return puesto;
	}
	
	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}
	
	public String getPerfil() {
		return perfil;
	}
	
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	
	@Override
	 public String toString() {
	  StringBuilder str = new StringBuilder();
	  str.append(" Usuario:- " + getuserName());
	  str.append(" Nombre usuario:- " + getNombre());
	  str.append(" Puesto:- " + getPuesto());
	  str.append(" Perfil:- " + getPerfil());
	  
	  return str.toString();
	  
	 }

}
