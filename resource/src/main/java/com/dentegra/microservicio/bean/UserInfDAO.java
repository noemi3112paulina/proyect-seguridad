package com.dentegra.microservicio.bean;

import java.util.Map;
import com.dentegra.microservicio.bean.UserInf;

public interface UserInfDAO {
	
	public UserInf getUserInf(String userName);
	public Map<String, Object> obtenerInformacionUsuario(String strUsuario);
	
}
