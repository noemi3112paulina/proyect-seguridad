package com.dentegra.microservicio.bean;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserInfServiceImp implements UserInfService {

	@Autowired
	private UserInfDAO userInfDao;
	
	public UserInf getUserInf(String userName) {
		UserInf userInf = userInfDao.getUserInf(userName);
		return userInf;
	}
	
	public Map<String, Object> obtenerInformacionUsuario(String userName) {
		Map<String, Object> userInf = userInfDao.obtenerInformacionUsuario(userName);
		return userInf;
	}
	
}
