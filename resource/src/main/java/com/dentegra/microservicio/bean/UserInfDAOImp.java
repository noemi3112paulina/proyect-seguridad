package com.dentegra.microservicio.bean;

import java.sql.Types;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.dentegra.microservicio.bean.UserInf;

@Repository
public class UserInfDAOImp implements UserInfDAO {
	
	@Value("${queries.usersByUsernameQuery}") private String usersByUsernameQuery;
	@Value("${queries.SPObtenerDatosLogin}") private String SPObtenerDatosLogin;
	@Value("${queries.strDBSchema}")  		  private String strDBSchema;
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	 public void setDataSource(DataSource dataSource) {
	  this.jdbcTemplate = new JdbcTemplate(dataSource);
	 }
	
	@Override
	public UserInf getUserInf(String userName) {
		UserInf userInf = null;
		try{
			
			//System.out.println("usersByUsernameQuery: "+usersByUsernameQuery);
			userInf = jdbcTemplate.queryForObject(usersByUsernameQuery, new Object[] {userName},new BeanPropertyRowMapper<UserInf>(UserInf.class));
			//System.out.println("usersByUsernameQuery: "+userInf);
			
		}
		catch (DataAccessException e) {
			   e.printStackTrace();
		  }
		
		
		return userInf;
	}
	@Override
	public Map<String, Object> obtenerInformacionUsuario(String strUsuario) {
		SimpleJdbcCall call = new SimpleJdbcCall(this.jdbcTemplate).withSchemaName(strDBSchema)
	            .withProcedureName(SPObtenerDatosLogin)
	            .declareParameters(
	                    new SqlParameter("pUsuario", Types.NVARCHAR));
	    Map<String, Object> execute = call.execute(new MapSqlParameterSource().addValue("pUsuario", strUsuario));
	    return execute;
	}

}
