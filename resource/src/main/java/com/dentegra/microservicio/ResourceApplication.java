package com.dentegra.microservicio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**Para generar jar*/

@SpringBootApplication
public class ResourceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResourceApplication.class, args);
	}
}
/**Para generar war*/
/*
@SpringBootApplication
public class ResourceApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ResourceApplication.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ResourceApplication.class, args);
	}

}
*/