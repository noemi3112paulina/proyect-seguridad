package com.dentegra.microservicio.controller;

import java.security.Principal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dentegra.microservicio.bean.UserInf;
import com.dentegra.microservicio.bean.UserInfService;

@RestController
public class ResourseController {
		
	/*@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Map<String, Object> readFoo(@PathVariable Integer id, Principal principal){
		Map<String, Object> map = new HashMap<>();
		
		map.put("id", id);
		map.put("resource", UUID.randomUUID() + " as String" );
		map.put("user", principal.getName());
		
		return map;
	}*/
	
	@Autowired
	private UserInfService userInfService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<UserInf> getUserInf(Principal principal) {
			
			UserInf userInf = userInfService.getUserInf(principal.getName());
			//System.out.println(principal.getName());
			
			if (userInf == null){
				return new ResponseEntity<UserInf>(HttpStatus.NOT_FOUND); 
			}else{
				return new ResponseEntity<UserInf>(userInf,HttpStatus.OK);
			}
	}
	
	@RequestMapping(value = "/usr/", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> obtenerDatosUsuario(Principal principal) {
			
			Map<String, Object> userInf = userInfService.obtenerInformacionUsuario(principal.getName());
			//System.out.println(principal.getName());
			
			if (userInf == null){
				return new ResponseEntity<Map<String, Object>>(HttpStatus.NOT_FOUND); 
			}else{
				return new ResponseEntity<Map<String, Object>>(userInf,HttpStatus.OK);
			}
	}
	
}
