package com.dentegra.uaa.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

@Configuration
@EnableAuthorizationServer
public class OAuth2Configuration extends AuthorizationServerConfigurerAdapter {
	/* ClientDetailsServiceConfigurer: Define el servicio de detalles del cliente. Los detalles del cliente 
	 * pueden inicializarse, o puede referirse a una tienda existente.	*/
	@Autowired
    @Qualifier("authenticationManagerBean")
	AuthenticationManager authenticationManager;
	
	@Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient("zuul-client")
                .secret("zuul-secret") //secret: (requerido para clientes de confianza) el secret del cliente, si corresponde.
                .scopes("read")  //scope: El alcance al cual el cliente está limitado. 
                .autoApprove(true)
                .accessTokenValiditySeconds(600)
                .refreshTokenValiditySeconds(600)
                .authorizedGrantTypes("implicit", "refresh_token", "password", "authorization_code") //authorizedGrantTypes: Tipos de concesión autorizados para que el cliente los use. El valor predeterminado está vacío.
                .and()
                .withClient("client1")
                .secret("cl13n71")
                .scopes("read")
                .autoApprove(true)
                .accessTokenValiditySeconds(600)
                .refreshTokenValiditySeconds(600)
                .authorizedGrantTypes("implicit", "refresh_token", "password", "authorization_code","client_credentials");
	}
	
	/*
	 * AuthorizationServerEndpointsConfigurer: define los puntos finales de autorización y token y los servicios token.
	*/
	@Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.tokenStore(tokenStore()).tokenEnhancer(jwtTokenEnhancer()).authenticationManager(authenticationManager);
    }


    @Bean
    TokenStore tokenStore() {
        return new JwtTokenStore(jwtTokenEnhancer());
    }
    
    // JSON Web Token (JWT) 
    @Bean
    protected JwtAccessTokenConverter jwtTokenEnhancer() {
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new ClassPathResource("jwt.jks"), "kmzwa8awaa".toCharArray());
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setKeyPair(keyStoreKeyFactory.getKeyPair("jwt"));
        return converter;
    }

}
