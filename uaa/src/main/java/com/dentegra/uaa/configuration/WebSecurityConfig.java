package com.dentegra.uaa.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.dentegra.uaa.login.CustomUserDetailsService;

/*
 *Cls que se encarga de cuatenticar al usuario
 * */

/*@Configuration
@EnableWebSecurity
@EnableAutoConfiguration
@Order(-20)*/
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
    CustomUserDetailsService customUserDetailsService;
	
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		System.out.println("PAU en AuthenticationManager en la función authenticationManagerBean: ");
		return super.authenticationManagerBean();
	}
		
	@Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        System.out.println("PAU en WebSecurityConfig en la función bCryptPasswordEncoder: "+bCryptPasswordEncoder);
        return bCryptPasswordEncoder;
    }
	
	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception { 
	 
        // Serv que busca al usuario en la base de datos y coloca el PassswordEncoder
		System.out.println("PAU en configureGlobal en la función AuthenticationManagerBuilder: "+passwordEncoder());
		auth.userDetailsService(customUserDetailsService);     
 
	}
	
	@Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/css/**", "/fonts/**", "/img/**", "/js/**");
    }
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.formLogin().loginPage("/login").permitAll()
        .and().httpBasic().and()
        .requestMatchers()
        .antMatchers("/login", "/oauth/authorize", "/oauth/confirm_access")
        .antMatchers("/fonts/**", "/js/**", "/css/**")
        .and()
        .authorizeRequests()
        .antMatchers("/fonts/**", "/js/**", "/css/**").permitAll()
        .anyRequest().authenticated()
        .and().logout().permitAll().deleteCookies("JSESSIONID").invalidateHttpSession(true);
		 	
	}
	
	/*@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication()
		.dataSource(dataSource)
		.usersByUsernameQuery(usersByUsernameQuery)
		.authoritiesByUsernameQuery(authorities);
	}*/
	
	
	
}
