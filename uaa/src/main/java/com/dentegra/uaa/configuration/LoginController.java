package com.dentegra.uaa.configuration;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/*
 * Clase encargada de mapear las rutas de oauth2
 * unicamente tenemos la página de login
 * */
@Controller
public class LoginController {
	
	@RequestMapping(value="/login")
    public String login(){
        return "login";
    }
	
}
