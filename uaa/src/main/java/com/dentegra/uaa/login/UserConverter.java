package com.dentegra.uaa.login;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class UserConverter implements Converter<UserInfo, UserDetails> {
	
	@Override
	public UserDetails convert(UserInfo infoUsuario) {
		
		List<String> roleNames = infoUsuario.getRoles();
		
		List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
        if (roleNames != null) {
            for (String role : roleNames) {
                // ROLE_USER, ROLE_ADMIN,..
                GrantedAuthority authority = new SimpleGrantedAuthority(role);
                grantList.add(authority);
            }
        }
		
		UserDetails userDetails = (UserDetails)new User(infoUsuario.getUsername()
				, infoUsuario.getPassword()
				, grantList);
		return userDetails;
	}
}
