package com.dentegra.uaa.login;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAO {
	@Value("${queries.usersByUsernameQuery}")
	private String usersByUsernameQuery;

	@Value("${queries.authorities}")
	private String authorities;

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public UserInfo getUserInfo(String username) {

		UserInfo userInfo = (UserInfo) jdbcTemplate.queryForObject(usersByUsernameQuery, new Object[] { username },
				new RowMapper<UserInfo>() {
					public UserInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
						UserInfo user = new UserInfo();
						user.setUsername(rs.getString("username"));
						user.setPassword(rs.getString("password"));
						return user;
					}
				});
		return userInfo;

	}

	public List<String> getRoles(String username) {

		Object[] params = new Object[] { username };
		List<String> roles = this.jdbcTemplate.queryForList(authorities, params, String.class);
		return roles;

	}

}
